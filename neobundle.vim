function! objects#neobundle#New()


    let object = {}



    let object.lazy             = 0
    let object.autoload         = 0
    let object.insert           = 0
    let object.filetypes        = []
    let object.commands         = {}
    let object.commands.name    = ''
    let object.commands.complete = ''
    let object.commands.list    = ''
    let object.functions        = ''
    let object.function_ref     = ''
    let object.mappings         = []
    let object.unite_sources    = ''
    let object.explorer         = ''

    function! object.createCommandConfigFormat(text, kind) dict
        return a:text !=? {}
        \       ? "{ '" . a:kind . "' : '" . a:text . "' }, "
        \       : ''
    endfunction


    function! object.getCommandsStr() dict
        if len(self.commands.list) > 0
            return ''
        endif
        return "'commands' : ["
        \       .   self.getCommandsdNameStr()
        \       .   self.getCommandsdNameStr()
        \       .   self.quoteJoin(self.commands.list)
        \       .   "]"
    endfunction

    function! object.quoteJoin(list, delimiter)
        let retStr = ''
        for l:val in a:list
            let retStr .= "'" . retList . "'" . a:delimiter
        endfor

        return retStr
    endfunction




    function! object.getCommandsdNameStr() dict
        return self.createCommandConfigFormat(self.commands.name)
    endfunction


    function! object.getCommandsdNameStr() dict
        return self.createCommandConfigFormat(self.commands.complete)
    endfunction


    function! object.getLazyStr() dict
        return self.lazy   ==? 1 ? "'lazy' : 1, "  : ''
    endfunction




    function! object.getInsertStr() dict
        return self.insert ==? 1 ? "'insert' : 1, " : ''
    endfunction


    function! object.getFunctionsStr() dict
        return self.createConfigFormat('functions', self.functions)
    endfunction




    function! object.getUniteSourcesStr() dict
        return self.createConfigFormat('unite_sources', self.unite_sources)
    endfunction




    function! object.getMappingsStr() dict
        return self.createConfigFormat('mappings', self.mappings)
    endfunction




    function! object.createConfigFormat(kind, targetList)
        return len(a:targetList) > 0
        \           ? "'" . a:kind "' : " . string(a:targetList) . ', '
        \           : ''
    endfunction



    function! object.createNeoBundelConfig()

    endfunction

    return object
endfunction
